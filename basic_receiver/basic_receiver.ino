/*  ============================================================================

     MrDIY ULP Trigger Sensors

     THE RECEIVER

  ============================================================================= */

#include <ESP8266WiFi.h>
#include <espnow.h>
#include <ArduinoOTA.h>
uint8_t  fixedAddress[]      = {0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC}; // locally managed macAddress
byte     current_status;      // 0=offline , 1=online    ,2=OTA
#define  activity_pin   4
int      activity_on_timer;
//int     rssi_value;

/* ############################ Setup ############################################ */

void setup() {

  Serial.begin(115200);
  pinMode(activity_pin, OUTPUT);
  digitalWrite(activity_pin, HIGH);
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  wifi_set_macaddr(STATION_IF, &fixedAddress[0]);
  if (esp_now_init() != 0) {
    current_status = 0;
    sendCurrentStatus();
    delay(1000);
    ESP.restart();
  }
  ArduinoOTA.begin();
  ArduinoOTA.onEnd([]() {
    ESP.restart();
  });
  esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
  esp_now_register_recv_cb(OnDataRecv);
  //Serial.printf("Sending from %s\n", WiFi.softAPmacAddress().c_str());
  //Serial.printf("Listening on = %s\n", WiFi.macAddress().c_str());
  current_status = 1;
  sendCurrentStatus();
  activity_on_timer = 10000;
}
/*
  void promiscuous_rx_cb(void *buf, wifi_promiscuous_pkt_type_t type) {
  // All espnow traffic uses action frames which are a subtype of the mgmnt frames so filter out everything else.
  if (type != WIFI_PKT_MGMT)
    return;

  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buf;
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;

  int rssi = ppkt->rx_ctrl.rssi;
  rssi_value = rssi;
  }
*/

/* ############################ Loop ############################################# */

void loop() {

  if (Serial.available()) {
    char c = Serial.read();
    if ( c == '0' ) sendCurrentStatus();
    if ( c == '1' ) ESP.restart();
    if ( c == '2' ) startOTA();
  }
  if (current_status == 2) ArduinoOTA.handle();

  if ( activity_on_timer > 0) activity_on_timer--;
  if ( activity_on_timer == 0) digitalWrite(activity_pin, LOW);
}

void sendCurrentStatus() {
  Serial.println(current_status);
}

void startOTA() {
  WiFi.disconnect();
  WiFi.mode(WIFI_AP);
  WiFi.softAP("MrDIY Receiver", "mrdiy.ca");
  current_status = 2;
  sendCurrentStatus();
}

void OnDataRecv(uint8_t *mac_addr, uint8_t *incomingData, uint8_t len) {
  digitalWrite(activity_pin, HIGH);
  Serial.write(incomingData, len);
  Serial.write('\n');
  activity_on_timer = 3000;
}
