/*  ============================================================================

     MrDIY.ca ULP Trigger Sensors (Switch & Motion)

     THE SENDER

                       ESP12
                      --------------
                     | RST       TX |
         volt div----| ADC       RX |
                     | CH        05 |<-- switch
                     | 16        04 |--done-->
                     | 14        00 |
                     | 12        02 |
                     | 13        15 |
                     | V        GND |
                      --------------

   ============================================================================= */

//#define DEBUG_FLAG
//#define TEST_NODE

#include <ESP8266WiFi.h>
#include <espnow.h>

#define MESH_ID        6734922
#define GROUP_SWITCH   1
#define GROUP_HT       2
#define GROUP_MOTION   3

#define   donePin      4
#define   switchPin    5
#define   ledPin       1
#define   longest_up_time   5000

uint8_t   receiverAddress[]      = {0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC};
bool      ack_received;

typedef struct struct_message {
  int     mesh_id;
  uint8_t sensor_id[6];
  byte    category;
  bool    status;
  float   temperature;
  float   humidity;
  float   battery;
} struct_message;

struct_message msg;

/* ############################ Setup ############################################ */

void setup() {

  pinMode(switchPin,  INPUT);
  pinMode(ledPin,     OUTPUT);
  digitalWrite(donePin, HIGH);
  pinMode(donePin,    OUTPUT);
  digitalWrite(ledPin,  LOW);

  ack_received = false;

  WiFi.mode(WIFI_STA);
  if (esp_now_init() != 0) {
    delay(100);
    ESP.restart();
  }
  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_register_send_cb(OnDataSent);
  esp_now_add_peer(receiverAddress, ESP_NOW_ROLE_SLAVE, 1, NULL, 0);
  sendReading();

}

/* ################################### Loop ##################################### */

void loop() {

#ifdef TEST_NODE
  sendReading();
  delay(5000);
#else
  if ( ack_received == true)   gotoSleep();
#endif
  //if ( millis() >= longest_up_time)   gotoSleep();
}

void sendReading() {

  msg.mesh_id = MESH_ID;
  msg.category = GROUP_SWITCH; // GROUP_SWITCH or GROUP_MOTION
  WiFi.macAddress(msg.sensor_id);
  msg.status = digitalRead(switchPin);
  msg.battery =  analogRead(A0) * 4.2 / 1023;   // voltage divider
#ifdef TEST_NODE
  msg.status = random(0, 2);
  msg.battery = 3.5;
#endif
  esp_now_send(receiverAddress, (uint8_t *) &msg, sizeof(msg));
}

void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {

  ack_received = true;
}

void gotoSleep() {

  digitalWrite(donePin, LOW);
  delay(10);
  ESP.deepSleep(0);
}
