/*  ============================================================================

     MrDIY ULP Trigger Sensors System

     THE ATTINY

                      ------\/------
            (RESET)  | 1 PB5  VCC 8 |----(VCC)
       LDO enable<---| 2 PB3  PB2 7 |<---done signall from esp8266
                     | 3 PB4  PB1 6 |<---switch input (INT0/PCINT1)
            (GND)----| 4 GND  PB0 5 |
                     --------------

  ============================================================================= */
#include <avr/sleep.h>

#define SWITCH_INPUT_PIN           1              // Interrupt 
#define DONE_CONFIRMATION_PIN      2              // INPUT
#define POWER_TO_ESP_PIN           3              // OUTPUT

/* ############################ Setup ############################################ */


ISR(INT0_vect) {

  digitalWrite(POWER_TO_ESP_PIN, LOW);       // reset ESP8266
  digitalWrite(POWER_TO_ESP_PIN, HIGH);     // Turn on the ESP8266
}

void setup() {

  for (byte pin = 0; pin <= 20; pin++) {
    if ( pin != SWITCH_INPUT_PIN && pin != DONE_CONFIRMATION_PIN) {
      pinMode (pin, OUTPUT);
      digitalWrite (pin, LOW);
    }
  }
  pinMode(POWER_TO_ESP_PIN,       OUTPUT);
  pinMode(SWITCH_INPUT_PIN,       INPUT);
  pinMode(DONE_CONFIRMATION_PIN,  INPUT_PULLUP);
  digitalWrite(POWER_TO_ESP_PIN,  HIGH);
}

/* ################################### Loop ##################################### */

void loop() {

  if ( digitalRead(DONE_CONFIRMATION_PIN) == LOW) nap();
}

void nap()  {

  digitalWrite(POWER_TO_ESP_PIN, LOW);
  GIMSK   = 0b00100000;   // Interrupts
  PCMSK   = 0b00000010;
  ADCSRA  = 0b00000000;   // ADC
  PRR     = 0b00000001;
  WDTCR   = 0b00000000;   // Watchdog Timer
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  cli();
  sleep_enable();
  BODCR   = 0b00000011;   // Brown-out Detector
  BODCR   = 0b00000010;
  sei();
  sleep_cpu();
  sleep_disable();
}
