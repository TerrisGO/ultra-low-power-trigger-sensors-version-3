/*  ============================================================================

     MrDIY ULP Trigger Sensors

     THE ATTINY

                      ------\/------
            (RESET)  | 1 PB5  VCC 8 |----(VCC)
       LDO enable<---| 2 PB3  PB2 7 |<---done signall from esp8266
                     | 3 PB4  PB1 6 |<---switch input (INT0/PCINT1)
            (GND)----| 4 GND  PB0 5 |
                     --------------

  ============================================================================= */

#include <avr/interrupt.h>
#include <avr/sleep.h>

#define DONE_CONFIRMATION_PIN      PB2                    // INPUT
#define POWER_TO_ESP_PIN           PB3                    // OUTPUT
#define FIVE_MINS                  40
#define FIFTEEN_MINS               120

/* ############################ Setup ############################################ */

void setup() {

  pinMode(POWER_TO_ESP_PIN, OUTPUT);
  pinMode(DONE_CONFIRMATION_PIN,  INPUT_PULLUP);
  sleepForEightSeconds();

}

/* ################################### Loop ##################################### */

void loop() {

  powerOnEsp();
  while ( !espIsDone() ) {}
  powerOffEsp();
  for (byte i = 0; i < FIFTEEN_MINS; i++) sleepForEightSeconds();

}

/* ################################### Sleep ##################################### */

void sleepForEightSeconds() {

  ADCSRA &= ~(1 << ADEN);             // ADC off
  ACSR |= (1 << ACD);                 // Analog comparator off
  WDTCR |= (1 << WDTIE);              // enable Watchdog interrupt
  WDTCR |= (1 << WDP3) | (0 << WDP2) | (0 << WDP1) | (1 << WDP0); // 8s
  wdt_reset();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // Configure sleep mode (power-down)
  cli();
  sleep_enable();
  BODCR   = 0b00000011;   // Brown-out Detector
  BODCR   = 0b00000010;
  sei();
  sleep_cpu();
  sleep_disable();
}


/* ################################### Interrupt ########################################## */

ISR(WDT_vect) {
  wdt_disable();
}

/* ###################################### Tools ########################################### */

void powerOffEsp() {
  digitalWrite(POWER_TO_ESP_PIN, LOW);         // Turn off the ESP8266
}

void powerOnEsp() {
  digitalWrite(POWER_TO_ESP_PIN, LOW);                            // reset ESP8266
  digitalWrite(POWER_TO_ESP_PIN, HIGH);                           // Turn on the ESP8266                                        // new start time
}

bool espIsDone() {
  return !digitalRead(DONE_CONFIRMATION_PIN);    // LOW is true; pulled up
}
